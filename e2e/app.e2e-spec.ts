import { ChatikBooksAppPage } from './app.po';

describe('chatik-books-app App', () => {
  let page: ChatikBooksAppPage;

  beforeEach(() => {
    page = new ChatikBooksAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
