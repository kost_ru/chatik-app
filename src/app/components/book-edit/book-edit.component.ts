import {Component, Input, ViewChild, AfterViewInit, AfterContentChecked, SimpleChanges, OnChanges} from "@angular/core";
import {Book} from "../../shared/book";
import {BookService} from "../../services/book/book.service";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements AfterViewInit, OnChanges {

  @Input()
  public book: Book;

  public bookForm: FormGroup;

  constructor(private bookService: BookService, private fb: FormBuilder) {
    this.bookForm = fb.group({
      'title': [''],
      'author': ['']
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.reset();
  }

  public reset(): void {
    this.bookForm.reset();
    if (this.book != null) {
      this.bookForm.controls['title'].setValue(this.book.title);
      this.bookForm.controls['author'].setValue(this.book.author);
    }
  }

  public save(): void {
    let newBook: Book = BookService.clone(this.book);
    newBook.title = this.bookForm.controls['title'].value;
    newBook.author = this.bookForm.controls['author'].value;
    this.bookService.updateOpenedBook(newBook);
  }

  public close(): void {
    this.bookService.closeBook();
  }

  // textarea resize workaround
  @ViewChild('autoSizer') autoSizer;

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.autoSizer.resizeToFitContent();
    }, 10);
  }

}
