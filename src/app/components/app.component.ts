import {Component, OnInit, ViewChild, HostListener} from "@angular/core";
import {Book} from "../shared/book";
import {Mode} from "../shared/mode";
import {Device} from "../shared/device";
import {BookService} from "../services/book/book.service";
import {MdSidenav} from "@angular/material";
import {Observable, BehaviorSubject} from "rxjs";
import {AuthService} from "../services/auth/auth.service";

const PHONE_WIDTH = 600;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent implements OnInit {

  public mode: Mode = "card";
  public device: Device = "pc";
  public books: BehaviorSubject<Array<Book>>;
  public isOpened: boolean;
  public currentBook: Observable<Book>;

  @ViewChild('sidenav') sidenav: MdSidenav;

  constructor(private bookService: BookService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.bookService.init();
    this.books = this.bookService.books;
    this.bookService.isBookOpened.subscribe(isOpened => {
      this.isOpened = isOpened;
      this.syncSidenavState(isOpened);
    })
    this.currentBook = this.bookService.openedBook.asObservable();
    this.device = AppComponent.mapWidth(window.innerWidth);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.device = AppComponent.mapWidth(event.target.innerWidth);
  }

  private static mapWidth(width: number): Device {
    if (width <= PHONE_WIDTH) {
      return "phone";
    } else {
      return "pc";
    }
  }

  isPc(): boolean {
    return this.device == "pc";
  }

  public sidenavClose(): void {
    this.bookService.closeBook();
  }

  syncSidenavState(isOpened: boolean): void {
    if (isOpened) {
      this.sidenav.open();
    } else {
      this.sidenav.close();
    }
  }

  onToggle(mode) {
    this.mode = mode;
  }

  isTable(): boolean {
    return this.authService.authenticated() &&
      (this.mode == "table" || this.device == "phone");
  }

  isCard(): boolean {
    return this.authService.authenticated() &&
      (this.mode == "card" && this.device != "phone");
  }

}
