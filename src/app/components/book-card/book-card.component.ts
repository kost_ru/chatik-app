import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Book} from "../../shared/book";
import {BookService} from "../../services/book/book.service";

@Component({
  selector: 'book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.css']
})
export class BookCardComponent {

  @Input()
  public book: Book;

  constructor(private bookService: BookService) {
  }

  public openBook(): void {
    this.bookService.openBook(this.book);
  }

  public download(): void {
    window.open(this.book.webContentLink);
  }


}
