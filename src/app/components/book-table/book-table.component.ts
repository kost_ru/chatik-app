import {Component, Input, OnInit, OnChanges, SimpleChanges} from "@angular/core";
import {Book} from "../../shared/book";
import {
  TdDataTableSortingOrder, ITdDataTableSortChangeEvent, TdDataTableService,
  ITdDataTableColumn
} from "@covalent/core";
import {BookService} from "../../services/book/book.service";

@Component({
  selector: 'book-table',
  templateUrl: './book-table.component.html',
  styleUrls: ['./book-table.component.css']
})
export class BookTableComponent implements OnChanges{

  @Input()
  private books: Array<Book>;

  columns: ITdDataTableColumn[] = [
    { name: 'edit', label: ''},
    { name: 'title', label: 'Название'},
    { name: 'author', label: 'Автор'},
    { name: 'download', label: ''},
    { name: 'extension', label: 'Формат'},
    { name: 'ownerName', label: 'Владелец'},
  ];

  sortedData: Array<Book> = this.books;
  sortBy: string = 'title';
  sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Ascending;

  constructor(private _dataTableService: TdDataTableService, private bookService: BookService) {}

  ngOnChanges(changes: SimpleChanges) {
    this.update();
  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {
    this.sortBy = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.update();
  }

  update() {
    let newData: Array<Book> = this.books;
    newData = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
    this.sortedData = newData;
  }

  public openBook(book: Book): void {
    this.bookService.openBook(book);
  }

  public download(book: Book): void {
    window.open(book.webContentLink);
  }

}
