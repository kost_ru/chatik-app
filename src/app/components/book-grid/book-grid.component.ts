import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Book} from "../../shared/book";

@Component({
  selector: 'book-grid',
  templateUrl: './book-grid.component.html',
  styleUrls: ['./book-grid.component.css']
})
export class BookGridComponent {

  @Input()
  public books: Array<Book>;

  constructor() {
  }

}
