import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Mode} from "../../shared/mode";

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  @Input()
  private mode: Mode;

  @Input()
  private big: boolean;

  @Output()
  private toggle: EventEmitter<Mode> = new EventEmitter();

  onToggle(mode) {
    this.toggle.emit(mode);
  }

}
