import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarBigComponent } from './toolbar-big.component';

describe('ToolbarBigComponent', () => {
  let component: ToolbarBigComponent;
  let fixture: ComponentFixture<ToolbarBigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolbarBigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarBigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
