import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {AuthService} from "../../../services/auth/auth.service";
import {ToolbarBaseComponent} from "../toolbar-base/toolbar-base.component";
import {Mode} from "../../../shared/mode";

@Component({
  selector: 'toolbar-big',
  templateUrl: './toolbar-big.component.html',
  styleUrls: ['./toolbar-big.component.css']
})
export class ToolbarBigComponent extends ToolbarBaseComponent{

  @Input()
  private mode: Mode;

  @Output()
  private toggle: EventEmitter<Mode> = new EventEmitter();

  onToggle(mode) {
    this.toggle.emit(mode.value);
  }

}
