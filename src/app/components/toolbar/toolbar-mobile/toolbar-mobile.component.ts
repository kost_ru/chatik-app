import {Component, OnInit, ElementRef} from '@angular/core';
import {AuthService} from "../../../services/auth/auth.service";
import {Observable} from "rxjs";
import {BookService} from "../../../services/book/book.service";
import {ToolbarBaseComponent} from "../toolbar-base/toolbar-base.component";

@Component({
  selector: 'toolbar-mobile',
  templateUrl: './toolbar-mobile.component.html',
  styleUrls: ['./toolbar-mobile.component.css']
})
export class ToolbarMobileComponent extends ToolbarBaseComponent{

}
