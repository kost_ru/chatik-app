import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../services/auth/auth.service";
import {BookService} from "../../../services/book/book.service";

@Component({
  selector: 'toolbar-base',
  templateUrl: './toolbar-base.component.html',
  styleUrls: ['./toolbar-base.component.css']
})
export class ToolbarBaseComponent {

  constructor(private authService: AuthService,
              private booksService: BookService) { }


  search(searchTerm: string) {
    this.booksService.updateSearchTerm(searchTerm);
  }

  isLoggedIn(): boolean {
    return this.authService.authenticated();
  }

  login() {
    this.authService.login();
  }

  logout() {
    this.authService.logout();
  }

  userName(): string {
    return this.authService.getUserName();
  }

}
