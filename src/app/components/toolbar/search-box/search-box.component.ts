import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent  {

  private searchTerm: string;

  @Output()
  search: EventEmitter<string> = new EventEmitter();

  constructor() { }

  onSearch() {
    this.search.emit(this.searchTerm);
  }

  clear() {
    this.searchTerm = null;
    this.onSearch();
  }

}
