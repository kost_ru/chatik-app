import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule, Http, RequestOptions} from "@angular/http";
import {MaterialModule} from "@angular/material";
import "hammerjs";
import {AuthHttp, AuthConfig} from "angular2-jwt";
import {AppComponent} from "./components/app.component";
import {AuthService} from "./services/auth/auth.service";
import {DriveService} from "./services/drive/drive.service";
import {BookTableComponent} from "./components/book-table/book-table.component";
import {BookCardComponent} from "./components/book-card/book-card.component";
import {BookGridComponent} from "./components/book-grid/book-grid.component";
import {ToolbarComponent} from "./components/toolbar/toolbar.component";
import { BookEditComponent } from './components/book-edit/book-edit.component';
import {BookService} from "./services/book/book.service";
import { ToolbarMobileComponent } from './components/toolbar/toolbar-mobile/toolbar-mobile.component';
import { ToolbarBigComponent } from './components/toolbar/toolbar-big/toolbar-big.component';
import { SearchBoxComponent } from './components/toolbar/search-box/search-box.component';
import { ToolbarBaseComponent } from './components/toolbar/toolbar-base/toolbar-base.component';
import { CovalentCoreModule } from '@covalent/core';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({}), http, options);
}

@NgModule({
  declarations: [
    AppComponent,
    BookTableComponent,
    BookCardComponent,
    BookGridComponent,
    ToolbarComponent,
    BookEditComponent,
    ToolbarMobileComponent,
    ToolbarBigComponent,
    SearchBoxComponent,
    ToolbarBaseComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CovalentCoreModule.forRoot(),
  ],
  providers: [
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
    AuthService,
    DriveService,
    BookService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
