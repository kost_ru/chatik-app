interface AuthConfiguration {
    clientID: string,
    domain: string
}

export const myConfig: AuthConfiguration = {
    clientID: "d0oyXiVCZJFXJemyPIE4hVdpehww4ukC",
    domain: "chatik.eu.auth0.com"
};
