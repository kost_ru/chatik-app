/** Represents a normalized UserProfile. */
interface Auth0UserProfile {
  email: string;
  email_verified: boolean;
  family_name: string;
  gender: string;
  given_name: string;
  locale: string;
  name: string;
  nickname: string;
  picture: string;
  user_id: string;
  /** Represents one or more Identities that may be associated with the User. */
  identities: Auth0Identity[];
  user_metadata?: any;
  app_metadata?: any;
}

/** Represents multiple identities assigned to a user. */
interface Auth0Identity {
  access_token: string;
  connection: string;
  isSocial: boolean;
  provider: string;
  user_id: string;
}
