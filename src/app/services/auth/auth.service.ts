import {Injectable, NgZone} from "@angular/core";
import {tokenNotExpired} from "angular2-jwt";
import {myConfig} from "./auth.config";
import Auth0Lock from "auth0-lock";
import {Subject, Observable} from "rxjs";
import {ISubscription} from "rxjs/Subscription";


@Injectable()
export class AuthService {

  public folderIdSubject: Subject<string> = new Subject();
  private lock = new Auth0Lock(myConfig.clientID, myConfig.domain, {});
  private zone: NgZone;
  private userProfile: Auth0UserProfile;
  private authExpire: ISubscription;

  constructor(zone: NgZone) {
    this.zone = zone;
  }

  public init() {
    this.lock.on("authenticated", (authResult) => {
      localStorage.setItem("id_token", authResult.idToken);
      localStorage.setItem("userToken", authResult.accessToken);

      this.lock.getUserInfo(authResult.accessToken, (error, profile) => {
        if (error) {
          console.log(error);
          return;
        }
        console.log(JSON.stringify(profile))
        this.updateUserProfile(profile);
      });
    });

    this.authExpire = this.setupAuthExpire();
    let userProfile = JSON.parse(localStorage.getItem("profile"));
    if (userProfile != null) {
      this.updateUserProfile(userProfile);
    }
  }

  private setupAuthExpire(): ISubscription {
    return Observable.interval(1000).subscribe(x => {
      if (!this.authenticated()) {
        this.logout();
      }
    });
  }

  private updateUserProfile(userProfile: Auth0UserProfile) {
    if (userProfile.identities[0].provider === "google-oauth2") {
      this.userProfile = userProfile;
      localStorage.setItem("profile", JSON.stringify(userProfile));
      if (userProfile.app_metadata && userProfile.app_metadata.folderId) {
        console.log(userProfile.app_metadata.folderId)
        this.zone.run(() => this.folderIdSubject.next(userProfile.app_metadata.folderId));
      } else {
        console.log("You have no shared folders");
        alert("You have no shared folders");
      }
    } else {
      this.logout();
      alert("Only google-oauth2 is enabled!");
      console.log("You have no shared folders");
    }
  }

  public login() {
    this.lock.show();
  };

  public authenticated() {
    return tokenNotExpired();
  };

  public logout() {
    localStorage.removeItem("id_token");
    localStorage.removeItem("userToken");
    localStorage.removeItem("profile");
    this.folderIdSubject.next(null);
    this.authExpire.unsubscribe();
  };

  public getUserName(): string {
    if (this.userProfile != null) {
      return this.userProfile.name;
    } else {
      return "";
    }
  }
}

