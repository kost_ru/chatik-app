import {Injectable} from "@angular/core";
import {DriveService} from "../drive/drive.service";
import {Book} from "../../shared/book";
import {AuthService} from "../auth/auth.service";
import {BehaviorSubject, Subject} from "rxjs";

@Injectable()
export class BookService {

  public books: BehaviorSubject<Array<Book>> = new BehaviorSubject<Array<Book>>(null);
  public openedBook: BehaviorSubject<Book> = new BehaviorSubject<Book>(null);
  public isBookOpened: Subject<boolean> = new Subject<boolean>();
  private searchTerm: string;

  constructor(private driveService: DriveService, private authService: AuthService) {
    this.driveService.booksSubject.subscribe((books: Array<Book>) => this.updateResults(books));
  }

  public init(): void {
    this.authService.init();
    this.driveService.init();
  }

  public openBook(book: Book) {
    this.openedBook.next(BookService.clone(book));
    this.isBookOpened.next(true);
  }

  public closeBook() {
    this.openedBook.next(null);
    this.isBookOpened.next(false);
  }

  public updateOpenedBook(book: Book) {
    let originalBook: Book = this.books.getValue().find(b => b.id == book.id);
    this.driveService.updateBook(book).then(success => {
      if (success) {
        BookService.sync(book, originalBook);
        this.openedBook.next(book);
      }
    });
  }

  public static clone(object): Book {
    return JSON.parse(JSON.stringify(object));
  }

  private static sync(source: Book, target: Book): void {
    target.title = source.title;
    target.author = source.author;
  }

  public updateSearchTerm(searchTerm: string): void {
    this.searchTerm = searchTerm;
    this.updateResults(this.driveService.booksSubject.getValue());
  }

  private updateResults(books: Array<Book>): void {
    if (books != null) {
      let newBooksResult: Array<Book> = books.filter((b: Book) => BookService.find(b, this.searchTerm));
      // this.books.length = 0;
      // this.books.push(...newBooksResult);
      this.books.next(newBooksResult);
    }
  }

  private static find(b: Book, search: string) {
    if (search != null && search.length > 0) {
      let titleMatches = BookService.strMatch(b.title, search);
      let authorMatches = BookService.strMatch(b.author, search);
      return titleMatches || authorMatches;
    }
    return true;
  }

  private static strMatch(str: string, substr: string): boolean {
    if (str != null) {
      return str.toUpperCase().indexOf(substr.toUpperCase()) != -1;
    }
    return false;
  }

}
