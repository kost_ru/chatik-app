import {Injectable, NgZone} from "@angular/core";
import {Subject, BehaviorSubject} from "rxjs";
import {Book} from "../../shared/book";
import {AuthService} from "../auth/auth.service";
import {MdSnackBar, MdSnackBarConfig} from "@angular/material";

declare var gapi: any;

const GOOGLE_APIKEY: string = "AIzaSyBgpTzUhIZo0AigmBJ406AqZfbJHmp1N5g";
const GOOGLE_CLIENTID: string = "69355589948-2e8cugv8ierpirbkn397pgoih1vcnaet.apps.googleusercontent.com";
const GOOGLE_SCOPES: string = "https://www.googleapis.com/auth/drive";
const GOOGLE_DISCOVERY_DOCS: string = "https://www.googleapis.com/discovery/v1/apis/drive/v3/rest";

@Injectable()
export class DriveService {

  public booksSubject: BehaviorSubject<Array<Book>> = new BehaviorSubject(null);
  private folderId: string = null;
  private isGapiInit: boolean = false;
  private zone: NgZone;

  constructor(zone: NgZone, authService: AuthService, private snackBar: MdSnackBar) {
    this.zone = zone;
    authService.folderIdSubject.subscribe(id => {
      this.folderId = id;
      this.onFolderIdChanged();
    })
  }

  public init() {
    this.loadGapi();
  }

  private loadGapi() {
    gapi.load("client", () => {
      gapi.client.init({
        "apiKey": GOOGLE_APIKEY,
        "discoveryDocs": [GOOGLE_DISCOVERY_DOCS]
      }).then(() => {
        this.isGapiInit = true;
        if (this.folderId != null) {
          this.onFolderIdChanged();
        }
      });
    });

  }

  private onFolderIdChanged() {
    if (this.isGapiInit && this.folderId != null) {
      this.refreshGoogleToken().then(success => {
        if (success) {
          this.reloadBooks();
        }
      });
    } else {
      this.booksSubject.next(new Array<Book>());
    }
  }

  private refreshGoogleToken(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      gapi.auth.authorize({
        client_id: GOOGLE_CLIENTID,
        scope: GOOGLE_SCOPES,
        immediate: true
      }, authResult => {
        console.log(JSON.stringify(authResult))
        if (authResult) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }

  public reloadBooks() {
    gapi.client.drive.files.list({
      q: `'${this.folderId}' in parents`,
      fields: "files(id, name, fileExtension, properties, owners, webContentLink, iconLink, thumbnailLink)"
    }).then(response => {
        let files: any = response.result.files;
        console.log(JSON.stringify(files).substring(0, 100))
        if (files && files.length > 0) {
          let books: Array<Book> = files.map(f => new Book({
                id: f.id,
                name: f.name,
                extension: f.fileExtension,
                author: f.properties && f.properties.author || null,
                title: DriveService.mergeTitleProperty(f.properties),
                ownerName: f.owners[0].displayName,
                ownerEmail: f.owners[0].emailAddress,
                ownerPhotoLink: f.owners[0].photoLink,
                webContentLink: f.webContentLink,
                iconLink: f.iconLink,
                thumbnailLink: f.thumbnailLink
              }
            ));
          // var url = 'data:text/json;charset=utf8,' + encodeURIComponent(JSON.stringify(books));
          // window.open(url, '_blank');
          // window.focus();
          books.sort((b1, b2) => {
            if (b1.title < b2.title) return -1;
            if (b1.title > b2.title) return 1;
            return 0;
          })
          console.log(JSON.stringify(books).substring(0, 100))
          this.zone.run(() => this.booksSubject.next(books));
        }
      }
    );
  }

  public updateBook(book: Book): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      gapi.client.drive.files.update({
        'fileId': book.id,
        'properties': DriveService.createProperties(book)
      }).execute(resp => {
        let config = new MdSnackBarConfig();
        config.duration = 3000;
        if ('error' in resp) {
          this.snackBar.open(resp.error.message, null, config);
          resolve(false);
        } else {
          this.snackBar.open("Сохранено", null, config);
          resolve(true);
        }
      });
    });
  }

  private static createProperties(book: Book) {
    let titles: Array<string> = DriveService.chunkString(book.title, 60);
    let properties: any = {};
    properties.author = book.author;
    for (let i = 0; i < titles.length; i++) {
      if (i == 0) {
        properties.title = titles[0];
      } else {
        properties["title_" + i] = titles[i];
      }
    }
    return properties;
  }

  private static mergeTitleProperty(properties: any): string {
    if (properties && properties.title) {
      let title = properties.title;
      let i = 1;
      while (i < 42) {
        if ("title_" + i in properties) {
          title += properties["title_" + i];
          i++;
        } else {
          break;
        }
      }
      return title;
    } else {
      return null;
    }

  }

  private static chunkString(str: string, length: number): Array<string> {
    return str.match(new RegExp("(.|[\\s\\S]){1," + length + "}", "g"));
  }


}
