export class Book {
  id: string;
  name: string;
  extension: string;
  author: string;
  title: string;
  ownerName: string;
  ownerEmail: string;
  ownerPhotoLink: string;
  webContentLink: string;
  iconLink: string;
  thumbnailLink: string;

  constructor(obj?: any) {
    this.id               = obj && obj.id                 || null;
    this.name             = obj && obj.name               || null;
    this.extension        = Book.extractExtension(obj);
    this.author           = obj && obj.author             || null;
    this.title            = obj && obj.title              || this.name;
    this.ownerName        = obj && obj.ownerName          || null;
    this.ownerEmail       = obj && obj.ownerEmail         || null;
    this.ownerPhotoLink   = obj && obj.ownerPhotoLink     || null;
    this.webContentLink   = obj && obj.webContentLink     || null;
    this.iconLink         = obj && obj.iconLink           || null;
    this.thumbnailLink    = Book.extractThumbnailInk(obj);
  }

  private static extractExtension(obj?: any): string {
    let name: string = obj && obj.name || null;
    let ext: string = obj && obj.extension || null;
    if (name && name.lastIndexOf(".") != -1) {
      return name.substring(name.lastIndexOf(".") + 1, name.length);
    } else {
      return ext;
    }
  }

  private static extractThumbnailInk(obj?: any): string {
   return obj && obj.thumbnailLink || ("assets/icons/" + Book.extractExtension(obj) + ".png" || null);
  }
}
