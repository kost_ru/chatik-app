# ChatikBooksApp

##Why

I've started developing this small webapp, because I have a bunch of friends that share ebooks on google drive.
Unfortunately, the default google.drive interface is not suited for this (search is global, there are no title/author attributes etc.)

##How
There is no backend, only Angular2-based webapp, that is using google.drive REST-services (via gapi js library).
In order to restrict the access, I've integrated the app with auth0.com (OAuth provider/aggregator).
In the real app the **FolderID** will be sent as a part of enriched auth0.com user profile (I basically add it manually -- https://auth0.com/docs/metadata)
In the example app (http://kostru.1apps.com) the **FolderID** is hardcoded in auth config.

##Tech stack
1. Angular 2
2. Angular Material 2
3. Teradata Datatable
4. gapi.js
5. a couple of auth0.com libs
6. angular-cli
7. yarn

##TODOs
1. Data architecture:
Currently the app's state is distributed throughout the whole application -- have the state in a single place (ngrx/store, angular2-redux or something self-made)
2. Routing:
The app state is not connected to url (e.g. presentation mode, which books is being edited etc.). I even hope that will fix the bug on mobile (the app is reloaded after the closing of side panel)
3. Upload!
4. Fix mobile version.
5. Try to optimize the size (right now it's 6MB)
